package vn.panota.base.job;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import org.apache.commons.lang3.Validate;
import vn.panota.base.Constants.JobKey;
import vn.panota.base.Constants.SharedDataName;
import vn.panota.base.cache.ShareableCacheHolder;
import vn.panota.base.util.DateTimeUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class AbstractBaseJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBaseJob.class);

    protected Vertx vertx;
    protected ShareableCacheHolder cacheHolder;

    protected int FIRST_DAY = 1546279200;
    protected final String HOURLY = "hourly";
    protected final String DAILY = "daily";

    protected AbstractBaseJob(Vertx vertx) {
        this.vertx = vertx;
        SharedData sharedData = vertx.sharedData();
        LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(SharedDataName.SHARED_MODULAR_LOCAL);
        this.cacheHolder = (ShareableCacheHolder) localDataAccess.get(SharedDataName.SHARED_MODULAR_CACHE_NAME);
    }

    protected List<Integer[]> initScheduledJob(JsonObject jobParams) throws ParseException {
        Validate.notNull(jobParams, "Job Params object should not be null");

        int timeFrame = jobParams.getString("time_frame", JobKey.HOURLY).equals(JobKey.HOURLY) ? Calendar.HOUR_OF_DAY : Calendar.DATE;
        Calendar fromCal = Calendar.getInstance();
        Calendar toCal = Calendar.getInstance();
        if (jobParams.containsKey(JobKey.LAST_RUN_TIME)) {
            fromCal.setTime(this.toTime(jobParams.getValue(JobKey.LAST_RUN_TIME)));
            toCal = DateTimeUtil.truncateCalendar(toCal, timeFrame);
        } else {
            if (jobParams.containsKey(JobKey.FROM_TIME))
                fromCal.setTime(this.toTime(jobParams.getString(JobKey.FROM_TIME)));
            else fromCal = DateTimeUtil.truncateCalendar(fromCal, timeFrame);

            if (jobParams.containsKey(JobKey.TO_TIME)) toCal.setTime(this.toTime(jobParams.getString(JobKey.TO_TIME)));
            else toCal = DateTimeUtil.truncateCalendar(toCal, timeFrame);
        }
        List<Integer[]> result = new ArrayList<>();
        while (fromCal.compareTo(toCal) < 0) {
            int from = (int) (fromCal.getTimeInMillis() / 1000);
            fromCal.add(timeFrame, 1);
            int to = (int) (fromCal.getTimeInMillis() / 1000);
            result.add(new Integer[]{from, to});
        }
        return result;
    }

    protected Date toTime(Object timeObject) throws ParseException {
        Calendar cal = Calendar.getInstance();
        if (timeObject instanceof Integer) cal.setTimeInMillis(((int) timeObject) * 1000L);
        else if (timeObject instanceof Long) cal.setTimeInMillis((long) timeObject);
        else if (timeObject instanceof String) {
            String timeStr = (String) timeObject;
            try {
                Date date = DateTimeUtil.parseDate(timeStr, DateTimeUtil.YYYYMMDDHH_DASH);
                cal.setTime(date);
            } catch (ParseException e) {
                Date date = DateTimeUtil.parseDate(timeStr, DateTimeUtil.YYYYMMDD_DASH);
                cal.setTime(date);
            }
        }
        return cal.getTime();
    }
}
