package vn.panota.base.bridge.mongodb;

import io.netty.util.internal.StringUtil;
import io.vertx.core.json.JsonObject;


public class AggStages {

    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/addFields/
     * @param fields
     * @return
     */
    public static JsonObject addFields(JsonObject fields) {
        return Cmd.empty().put("$addFields", fields);
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/match/
     * @param value
     * @return
     */
    public static JsonObject match(JsonObject value) {
        return new JsonObject().put("$match", value);
    }

    private static JsonObject match(String field, Object value) {
        JsonObject match = StringUtil.isNullOrEmpty(field) ? new JsonObject() :
                new JsonObject().put(field, value);
        return match(match);
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/lookup/
     * @param from
     * @param localField
     * @param foreignField
     * @param as
     * @return
     */
    public static JsonObject lookup(String from, String localField, String foreignField, String as) {
        return new JsonObject().put("$lookup", new JsonObject()
                .put("from", from)
                .put("localField", localField)
                .put("foreignField", foreignField)
                .put("as", as));
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/replaceRoot/
     * @param newRoot
     * @return
     */
    public static JsonObject replaceRoot(JsonObject newRoot) {
        return new JsonObject().put("$replaceRoot", new JsonObject().put("newRoot", newRoot));
    }

}
