package vn.panota.base.bridge.mongodb;

import io.vertx.core.json.JsonObject;

public class EvaluateQueryCmd {

    public static JsonObject contains(String field, String value) {
        return Cmd.empty().put(field, Cmd.empty().put("$regex", ".*" + value + ".*"));
    }
}
