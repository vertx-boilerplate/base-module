package vn.panota.base.bridge.mongodb;

import io.vertx.core.json.JsonObject;

import java.util.Collection;

/**
 * Created by thangpham on 22/12/2018.
 */
public class CompareQueryCmd {

    public static JsonObject eq(String field, Object value) {
        return Cmd.empty().put(field, Cmd.empty().put("$eq", value));
    }

    public static JsonObject gt(String field, Object value) {
        return Cmd.empty().put(field, Cmd.empty().put("$gt", value));
    }

    public static JsonObject gte(String field, Object value) {
        return Cmd.empty().put(field, Cmd.empty().put("$gte", value));
    }

    public static JsonObject in(String field, Collection values) {
        return Cmd.empty().put(field, Cmd.empty().put("$in", values));
    }

    public static JsonObject lt(String field, Object value) {
        return Cmd.empty().put(field, Cmd.empty().put("$lt", value));
    }

    public static JsonObject lte(String field, Object value) {
        return Cmd.empty().put(field, Cmd.empty().put("lte", value));
    }

    public static JsonObject ne(String field, Object value) {
        return Cmd.empty().put(field, Cmd.empty().put("ne", value));
    }

    public static JsonObject nin(String field, Object value) {
        return Cmd.empty().put(field, Cmd.empty().put("nin", value));
    }
}
