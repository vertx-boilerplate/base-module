package vn.panota.base.bridge.mongodb;

import io.vertx.core.json.JsonObject;

import java.time.Instant;
import java.util.Map;

/**
 * Created by thangpham on 08/12/2018.
 */
public class Cmd {

    public static JsonObject empty() {
        return new JsonObject();
    }

    public static JsonObject date(Instant instant) {
        return new JsonObject().put("$date", instant);
    }

    public static JsonObject oid(String stringId) {
        return new JsonObject().put("$oid", stringId);
    }

    public static JsonObject facet(JsonObject values) {
        return new JsonObject().put("$facet", values);
    }

    public static JsonObject facet(Map<String, Object> values) {
        return facet(new JsonObject(values));
    }

    public static JsonObject count(String name) {
        return new JsonObject().put("$count", name);
    }


    public static JsonObject sort(JsonObject value) {
        return new JsonObject().put("$sort", value);
    }

    public static JsonObject limit(int limit) {
        return new JsonObject().put("$limit", limit);
    }

    public static JsonObject skip(int skip) {
        return new JsonObject().put("$skip", skip);
    }

    public static JsonObject project(String[] includes, String[] excludes) {
        JsonObject projects = new JsonObject();
        if(null != includes && includes.length > 0) for(String include : includes) projects.put(include, 1);
        if(null != excludes && excludes.length > 0) for(String exclude : excludes) projects.put(exclude, 0);
        return projects;
    }

}
