package vn.panota.base.bridge.mongodb;

import io.vertx.core.json.JsonObject;

public class FieldUpdate {

    /**
     * https://docs.mongodb.com/manual/reference/operator/update/set/
     * @param data
     * @return
     */
    public static JsonObject set(JsonObject data) {
        return Cmd.empty().put("$set", data);
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/update/unset/
     * @param data
     * @return
     */
    public static JsonObject unset(JsonObject data) { return Cmd.empty().put("$unset", data); }

    /**
     * https://docs.mongodb.com/manual/reference/operator/update/inc/
     * @param data
     * @return
     */
    public static JsonObject inc(JsonObject data) {
        return Cmd.empty().put("$inc", data);
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/update/setOnInsert/
     * @param data
     * @return
     */
    public static JsonObject setOnInsert(JsonObject data) { return Cmd.empty().put("$setOnInsert", data); }

    /**
     * https://docs.mongodb.com/manual/reference/operator/update/rename/
     * @param data
     * @return
     */
    public static JsonObject rename(JsonObject data) {
        return Cmd.empty().put("$rename", data);
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/update/mul/
     * @param data
     * @return
     */
    public static JsonObject mul(JsonObject data) {
        return Cmd.empty().put("$mul", data);
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/update/max/
     * @param data
     * @return
     */
    public static JsonObject max(JsonObject data) {
        return Cmd.empty().put("$max", data);
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/update/min/
     * @param data
     * @return
     */
    public static JsonObject min(JsonObject data) {
        return Cmd.empty().put("$min", data);
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/update/currentDate/
     * @param data
     * @return
     */
    public static JsonObject currentDate(JsonObject data) {
        return Cmd.empty().put("$currentDate", data);
    }
}
