package vn.panota.base.bridge.mongodb;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Arrays;

public class AggOperators {

    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/eq/
     * @param key
     * @param value
     * @return
     */
    public static JsonObject eq(String key, Object value) {
        return Cmd.empty().put("$eq", new JsonArray().add(key).add(value));
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/gt/
     * @param key
     * @param value
     * @return
     */
    public static JsonObject gt(String key, Object value) {
        return Cmd.empty().put("$gt", new JsonArray().add(key).add(value));
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/gte/
     * @param key
     * @param value
     * @return
     */
    public static JsonObject gte(String key, Object value) {
        return Cmd.empty().put("$gte", new JsonArray().add(key).add(value));
    }



    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/unwind/
     * @param path
     * @param includeArrayIndex
     * @param preserveNullAndEmptyArrays
     * @return
     */
    public static JsonObject unwind(String path, String includeArrayIndex, boolean preserveNullAndEmptyArrays) {
        return new JsonObject().put("$unwind", new JsonObject()
                .put("path", path)
                .put("includeArrayIndex", includeArrayIndex)
                .put("preserveNullAndEmptyArrays", preserveNullAndEmptyArrays));
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/arrayElemAt/
     * @param path
     * @param index
     * @return
     */
    public static JsonObject arrayElemAt(String path, int index) {
        return new JsonObject().put("$arrayElemAt", new JsonArray().add(path).add(index));
    }

    /**
     * https://docs.mongodb.com/manual/reference/operator/aggregation/mergeObjects/
     * @param objects
     * @return
     */
    public static JsonObject mergeObjects(Object... objects) {
        if (objects.length == 1)
            return new JsonObject().put("$mergeObjects", objects[0]);
        else return new JsonObject().put("$mergeObjects", new JsonArray(Arrays.asList(objects)));
    }
}
