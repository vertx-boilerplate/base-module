package vn.panota.base.bridge.mongodb;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Arrays;
import java.util.List;

public class LogicalQueryCmd {


    public static JsonObject and(JsonObject... filters) {
        return Cmd.empty().put("$and", new JsonArray(Arrays.asList(filters)));
    }

    public static JsonObject and(List<JsonObject> filters) {
        return Cmd.empty().put("$and", new JsonArray(filters));
    }

    public static JsonObject not(String field, JsonObject filter) {
        return Cmd.empty().put(field, Cmd.empty().put("$not", filter));
    }

    public static JsonObject nor(JsonObject... filters) {
        return Cmd.empty().put("$nor", new JsonArray(Arrays.asList(filters)));
    }

    public static JsonObject nor(List<JsonObject> filters) {
        return Cmd.empty().put("$nor", new JsonArray(filters));
    }

    public static JsonObject or(JsonObject... filters) {
        return Cmd.empty().put("$or", new JsonArray(Arrays.asList(filters)));
    }

    public static JsonObject or(List<JsonObject> filters) {
        return Cmd.empty().put("$or", new JsonArray(filters));
    }

}
