package vn.panota.base.cache;

import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.Shareable;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.web.client.WebClient;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.redis.client.RedisAPI;
import vn.panota.base.bridge.mongodb.Cmd;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by thangpham on 01/12/2018.
 */
public class ShareableCacheHolder implements Shareable {

    private static ShareableCacheHolder _instance;

    public synchronized static ShareableCacheHolder _load() {
        if (null == _instance) _instance = new ShareableCacheHolder();
        return _instance;
    }

    private RedisAPI redisAPI;
    private WebClient webClient;
    private JsonObject appConfig;
    private KafkaProducer<String, JsonObject> kafkaProducer;
    private ConcurrentMap<String, MongoClient> mongoClients = new ConcurrentHashMap<>();
    private ConcurrentMap<String, SQLClient> sqlClients = new ConcurrentHashMap<>();
    private JsonObject metadata = Cmd.empty();

    public ShareableCacheHolder setSQLClient(String conName, SQLClient client) {
        this.sqlClients.put(conName, client);
        return this;
    }

    public SQLClient getSQLClient(String conName) {
        if (!this.sqlClients.containsKey(conName))
            throw new NullPointerException("SQlClient connection name: " + conName + " is not initialized yet");
        return this.sqlClients.get(conName);
    }

    public MongoClient getMongoClient(String conName) {
        if (!this.mongoClients.containsKey(conName))
            throw new NullPointerException("Mongodb connection name: " + conName + " is not initialized yet");
        return this.mongoClients.get(conName);
    }

    public ShareableCacheHolder setMongoClient(String conName, MongoClient mongoClient) {
        this.mongoClients.put(conName, mongoClient);
        return this;
    }

    public KafkaProducer<String, JsonObject> getKafkaProducer() {
        return kafkaProducer;
    }

    public ShareableCacheHolder setKafkaProducer(KafkaProducer<String, JsonObject> kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
        return this;
    }

    public JsonObject getMetadata() {
        return metadata;
    }

    public JsonObject getAppConfig() {
        return appConfig;
    }

    public ShareableCacheHolder setAppConfig(JsonObject appConfig) {
        this.appConfig = appConfig;
        return this;
    }

    public WebClient getWebClient() {
        return webClient;
    }

    public ShareableCacheHolder setWebClient(WebClient webClient) {
        this.webClient = webClient;
        return this;
    }

    public RedisAPI getRedisAPI() {
        return this.redisAPI;
    }

    public ShareableCacheHolder setRedisAPI(RedisAPI redisAPI) {
        this.redisAPI = redisAPI;
        return this;
    }

}
