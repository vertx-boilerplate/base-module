package vn.panota.base;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import vn.panota.base.annotation.Starter;
import vn.panota.base.cache.ShareableCacheHolder;
import vn.panota.base.option.Order;
import vn.panota.base.util.VertxUtil;

@Starter(Order.HIGHEST)
public class LocalCacheInitializer extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(LocalCacheInitializer.class);

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load cache configuration");
        vertx.executeBlocking(event -> {
            try {
                VertxUtil.setLocalCacheObject(vertx, Constants.SharedDataName.SHARED_MODULAR_LOCAL,
                        Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME, ShareableCacheHolder._load());
                LOGGER.info("LOAD CACHE SUCCESSFULLY");
                event.complete();
            } catch (Exception ex) {
                event.fail(ex);
            }
        }, handler);
    }
}
