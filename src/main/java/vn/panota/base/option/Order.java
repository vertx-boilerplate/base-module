package vn.panota.base.option;

public enum Order {

    HIGHEST,
    HIGH,
    MEDIUM,
    LOW,
    LOWEST
}
