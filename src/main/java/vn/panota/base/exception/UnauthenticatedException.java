package einvoice.api.exception;

public class UnauthenticatedException extends Exception {

    public UnauthenticatedException() {
        super();
    }

    public UnauthenticatedException(String errorMessage) {
        super(errorMessage);
    }
}
