package einvoice.api.exception;

public class UnexpectedResultException extends Exception {

    public UnexpectedResultException() {
        super();
    }

    public UnexpectedResultException(String errorMessage) {
        super(errorMessage);
    }
}
