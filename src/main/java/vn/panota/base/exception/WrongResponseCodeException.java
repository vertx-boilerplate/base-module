package einvoice.api.exception;

public class WrongResponseCodeException extends Exception {
    public WrongResponseCodeException() {
        super();
    }

    public WrongResponseCodeException(String errorMessage) {
        super(errorMessage);
    }
}
