package einvoice.api.exception;

public class UnauthorizedException extends Exception {

    public UnauthorizedException() {
        super();
    }

    public UnauthorizedException(String errorMessage) {
        super(errorMessage);
    }
}
