package vn.panota.base.connector;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;

public abstract class TclConnector extends AbstractVerticle {

    protected static Router router = null;

    public static Router getRouter() {
        return router;
    }

    public synchronized void setRouter(Router router) {
        if(null == TclConnector.router) TclConnector.router = router;
        else router.clear();
        System.out.println(TclConnector.router);
    }

    protected TclConnector() {
        setRouter(Router.router(vertx));
    }

}
