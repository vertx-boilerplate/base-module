package vn.panota.base.connector;

import io.vertx.core.Promise;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import vn.panota.base.Constants;
import vn.panota.base.annotation.Starter;
import vn.panota.base.option.Order;
import vn.panota.base.util.FileUtil;

@Starter(Order.HIGHEST)
public class HttpServerConnector extends TclConnector {

    private static Logger LOGGER = LoggerFactory.getLogger(HttpServerConnector.class);

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load HTTP-SERVER configuration");
        JsonObject appConfig = vertx.getOrCreateContext().config();
        String configFile = appConfig.getString("HTTP_SERVER_CONFIG", Constants.ConfigurationFile.HTTP_SERVER_CONFIG);
        FileUtil.getConfigRetriever(configFile, vertx, cfAr -> {
            if (cfAr.succeeded()) {
                JsonObject config = cfAr.result();
                if (config.containsKey("http-server")) config = config.getJsonObject("http-server");
                HttpServerOptions serverOptions = new HttpServerOptions(config);
                int port = config.getInteger("port");
                String host = config.getString("host");
                vertx.createHttpServer(serverOptions).requestHandler(router).listen(port, host);
                LOGGER.info("START HTTP-SERVER AT " + host + ":" + port + " SUCCESSFULLY");
                handler.complete();
            } else handler.fail(cfAr.cause());
        });
    }
}
