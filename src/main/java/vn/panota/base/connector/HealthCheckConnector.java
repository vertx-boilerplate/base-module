package vn.panota.base.connector;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.web.Router;
import vn.panota.base.annotation.Starter;
import vn.panota.base.option.Order;

@Starter(Order.HIGH)
public class HealthCheckConnector extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(HealthCheckConnector.class);

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load health checker");
        HealthCheckHandler healthCheckHandler = HealthCheckHandler.create(vertx);

        Router router = HttpServerConnector.getRouter();
        router.get("/health*").handler(healthCheckHandler);
    }
}
