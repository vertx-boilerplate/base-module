package vn.panota.base.connector;


import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLClient;
import vn.panota.base.Constants;
import vn.panota.base.annotation.Starter;
import vn.panota.base.cache.ShareableCacheHolder;
import vn.panota.base.option.Order;
import vn.panota.base.util.FileUtil;

import java.util.ArrayList;
import java.util.Set;

@Starter(Order.HIGHEST)
public class SqlDBConnector extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(SqlDBConnector.class);

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load SqlServer configuration");
        JsonObject appConfig = vertx.getOrCreateContext().config();
        String configFile = appConfig.getString("SQL_CONNECTION_CONFIG", Constants.ConfigurationFile.SQL_CONNECTION_CONFIG);
        FileUtil.getConfigRetriever(configFile, vertx, cfAr -> {
            if (cfAr.succeeded()) {
                JsonObject config = cfAr.result();
                SharedData sharedData = vertx.sharedData();
                try {
                    LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
                    ShareableCacheHolder cacheHolder = ShareableCacheHolder._load();
                    localDataAccess.put(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME, cacheHolder);
                    Set<String> strings = config.fieldNames();
                    ArrayList<Future> checkList = new ArrayList<>();
                    for (String key : strings) {
                        SQLClient client = JDBCClient.createShared(vertx, config.getJsonObject(key), key);
                        Promise<Void> promise = Promise.promise();
                        checkList.add(promise.future());
                        client.getConnection(checkAr -> {
                            if(checkAr.succeeded()) {
                                cacheHolder.setSQLClient(key, client);
                                promise.complete();
                            } else promise.fail(checkAr.cause());
                        });
                    }
                    CompositeFuture.all(checkList).setHandler(ar -> {
                        if(ar.succeeded()) {
                            handler.complete();
                            LOGGER.info("END loading Sql-databases configuration");
                        } else handler.handle(Future.failedFuture(ar.cause()));
                    });
                } catch (Exception ex) {
                    LOGGER.error("Could not make connection to sql-databases", ex);
                    handler.fail(ex.getCause());
                }
            } else handler.fail(cfAr.cause());
        });
    }
}
