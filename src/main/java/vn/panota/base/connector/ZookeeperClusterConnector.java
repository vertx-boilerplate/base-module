package vn.panota.base.connector;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import vn.panota.base.annotation.Starter;
import vn.panota.base.option.Order;

@Starter(Order.HIGHEST)
public class ZookeeperClusterConnector extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(ZookeeperClusterConnector.class);

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load Zookeeper Cluster configuration");

    }
}
