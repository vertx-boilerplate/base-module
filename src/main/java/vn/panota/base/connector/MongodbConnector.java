package vn.panota.base.connector;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.mongo.MongoClient;
import vn.panota.base.Constants;
import vn.panota.base.annotation.Starter;
import vn.panota.base.cache.ShareableCacheHolder;
import vn.panota.base.option.Order;
import vn.panota.base.util.FileUtil;

@Starter(Order.HIGHEST)
public class MongodbConnector extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(MongodbConnector.class);

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load MongoDB configuration");
        JsonObject appConfig = vertx.getOrCreateContext().config();
        String configFile = appConfig.getString("MONGODB_CONNECTION_CONFIG", Constants.ConfigurationFile.MONGODB_CONNECTION_CONFIG);
        FileUtil.getConfigRetriever(configFile, vertx, cfAr -> {
            if (cfAr.succeeded()) {
                JsonObject config = cfAr.result();
                SharedData sharedData = vertx.sharedData();
                LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
                ShareableCacheHolder cacheHolder = ShareableCacheHolder._load();
                localDataAccess.put(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME, cacheHolder);
                for(String key : config.fieldNames()) {
                    MongoClient mongoClient = MongoClient.createShared(vertx, config.getJsonObject(key));
                    cacheHolder.setMongoClient(key, mongoClient);
                }
                handler.complete();
            } else handler.fail(cfAr.cause());
            LOGGER.info("END loading MongoDB configuration");
        });
    }
}
