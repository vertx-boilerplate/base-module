package vn.panota.base.connector;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.net.SocketAddress;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.redis.client.Redis;
import io.vertx.redis.client.RedisAPI;
import io.vertx.redis.client.RedisClientType;
import io.vertx.redis.client.RedisOptions;
import vn.panota.base.Constants;
import vn.panota.base.annotation.Starter;
import vn.panota.base.cache.ShareableCacheHolder;
import vn.panota.base.option.Order;
import vn.panota.base.util.FileUtil;

@Starter(Order.HIGHEST)
public class RedisConnector extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(RedisConnector.class);

    @Override
    public void start(Promise<Void> promise) throws Exception {
        LOGGER.info("START to load Redis configuration");
        JsonObject appConfig = vertx.getOrCreateContext().config();
        String configFile = appConfig.getString("REDIS_CONFIG", Constants.ConfigurationFile.REDIS_CONFIG);
        FileUtil.getConfigRetriever(configFile, vertx, cfAr -> {
            if (cfAr.succeeded()) {
                JsonObject config = cfAr.result();
                RedisOptions options = new RedisOptions();
                try {
                    JsonArray instances = config.getJsonArray("instances");
                    options.setMaxWaitingHandlers(config.getInteger("max_waiting_handlers", 2048));
                    options.setType(RedisClientType.valueOf(config.getString("client_type", RedisClientType.STANDALONE.toString())));
                    for (int i = 0; i < instances.size(); i++) {
                        JsonObject instance = instances.getJsonObject(i);
                        options.addEndpoint(SocketAddress.inetSocketAddress(instance.getInteger("port"), instance.getString("host")));
                    }
                    Redis.createClient(vertx, options).connect(onConnect -> {
                        if (onConnect.succeeded()) {
                            Redis result = onConnect.result();
                            SharedData sharedData = vertx.sharedData();
                            LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
                            localDataAccess.put(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME, ShareableCacheHolder._load().setRedisAPI(RedisAPI.api(result)));
                            promise.complete();
                            LOGGER.info("END loading Redis configuration");
                        } else promise.fail(onConnect.cause());
                    });
                } catch (Exception ex) {
                    promise.fail(ex);
                }
            } else promise.fail(cfAr.cause());
        });
    }
}
