package vn.panota.base.connector;

import io.vertx.core.Promise;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.net.JksOptions;
import vn.panota.base.Constants;
import vn.panota.base.annotation.Starter;
import vn.panota.base.option.Order;
import vn.panota.base.util.FileUtil;

@Starter(Order.HIGHEST)
public class HttpsServerConnector extends TclConnector {

    private static Logger LOGGER = LoggerFactory.getLogger(HttpsServerConnector.class);

    public HttpsServerConnector() {
        super();
    }

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load HTTPS-SERVER configuration");
        JsonObject appConfig = vertx.getOrCreateContext().config();
        String configFile = appConfig.getString("HTTPS_SERVER_CONFIG", Constants.ConfigurationFile.HTTPS_SERVER_CONFIG);
        FileUtil.getConfigRetriever(configFile, vertx, cfAr -> {
            if (cfAr.succeeded()) {
                JsonObject config = cfAr.result();
                if (config.containsKey("https-server")) {
                    config = config.getJsonObject("https-server");
                }
                HttpServerOptions options = new HttpServerOptions(config)
                        .setKeyStoreOptions(new JksOptions().setPath("./config/test.jks").setPassword("123456"));
//                String host = config.getString("host", "localhost");
//                int port = config.getInteger("port", 443);
//                HttpServerOptions serverOptions = null;
//                if (config.containsKey("options")) {
//                    JsonObject options = config.getJsonObject("options");
//                    serverOptions = new HttpServerOptions(options);
//                    serverOptions
//                            .setSsl(true)
//                            .setKeyStoreOptions(new JksOptions().setPath("./config/keystore").setPassword("123456"));
//                } else serverOptions = new HttpServerOptions();
                System.out.println(options.toJson());
                vertx.createHttpServer(options).requestHandler(router).listen();
                LOGGER.info("START HTTPS-SERVER AT " + config.getString("host") + ":" + config.getInteger("port") + " SUCCESSFULLY");
                handler.complete();
            } else handler.fail(cfAr.cause());
        });
    }
}
