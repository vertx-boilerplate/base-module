package vn.panota.base.connector;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.kafka.client.producer.KafkaProducer;
import vn.panota.base.Constants;
import vn.panota.base.annotation.Starter;
import vn.panota.base.cache.ShareableCacheHolder;
import vn.panota.base.option.Order;
import vn.panota.base.util.FileUtil;

import java.util.Map;
import java.util.stream.Collectors;

@Starter(Order.HIGHEST)
public class KafkaProducerConnector extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(KafkaProducerConnector.class);

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load Kafka producer configuration");
        JsonObject appConfig = vertx.getOrCreateContext().config();
        String configFile = appConfig.getString("KAFKA_PRODUCER_CONFIG", Constants.ConfigurationFile.KAFKA_PRODUCER_CONFIG);
        FileUtil.getConfigRetriever(configFile, vertx, cfAr -> {
            if(cfAr.succeeded()) {
                Map<String, String> mapConfig = cfAr.result().fieldNames().stream().collect(Collectors.toMap(String::toString, Object::toString));
                KafkaProducer<String, JsonObject> kafkaProducer = KafkaProducer.create(vertx, mapConfig);
                SharedData sharedData = vertx.sharedData();
                LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
                localDataAccess.put(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME, ShareableCacheHolder._load().setKafkaProducer(kafkaProducer));
            } else handler.fail(cfAr.cause());
            LOGGER.info("End loading Kafka producer configuration");
        });
    }
}
