package vn.panota.base.connector;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.web.client.WebClient;
import vn.panota.base.Constants;
import vn.panota.base.annotation.Starter;
import vn.panota.base.cache.ShareableCacheHolder;
import vn.panota.base.option.Order;

@Starter(Order.HIGHEST)
public class HttpClientConnector extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(HttpClientConnector.class);

    private WebClient webClient;

    public WebClient getWebClient() {
        if(null == webClient) throw new NullPointerException("WebClient is not initialized yet");
        else return this.webClient;
    }

    @Override
    public void start(Promise<Void> handler) throws Exception {
        WebClient webClient = WebClient.create(vertx);
        SharedData sharedData = vertx.sharedData();
        LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
        localDataAccess.put(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME, ShareableCacheHolder._load().setWebClient(webClient));
        handler.complete();
    }
}
