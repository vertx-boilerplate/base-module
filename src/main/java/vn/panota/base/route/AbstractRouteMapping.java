package vn.panota.base.route;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

public abstract class AbstractRouteMapping {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRouteMapping.class);

    protected Vertx vertx;

    protected AbstractRouteMapping(Vertx vertx) {
        this.vertx = vertx;
    }

    protected void logging(RoutingContext context) {
        HttpServerRequest request = context.request();
        LOGGER.info("Calling " + request.uri());
        context.next();
    }
}
