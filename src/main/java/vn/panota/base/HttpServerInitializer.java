package vn.panota.base;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import vn.panota.base.annotation.Starter;
import vn.panota.base.option.Order;
import vn.panota.base.util.FileUtil;

@Deprecated
@Starter(Order.HIGHEST)
public class HttpServerInitializer extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(LocalCacheInitializer.class);

    private static Router router;

    public static Router getRouter() {
        return router;
    }

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load HTTP-SERVER configuration");
        router = Router.router(vertx);
        FileUtil.getConfigRetriever(Constants.ConfigurationFile.HTTP_SERVER_CONFIG, vertx, cfAr -> {
            if (cfAr.succeeded()) {
                JsonObject config = cfAr.result();
                String host = config.getString("host", "localhost");
                int port = config.getInteger("port", 8080);
                vertx.createHttpServer().requestHandler(router).listen(port, host);
                LOGGER.info("START HTTP-SERVER AT " + host + ":" + port + " SUCCESSFULLY");
                handler.complete();
            } else handler.fail(cfAr.cause());
        });
    }
}
