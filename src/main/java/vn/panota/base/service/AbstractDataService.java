package vn.panota.base.service;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.mongo.MongoClient;
import vn.panota.base.Constants;
import vn.panota.base.bridge.mongodb.Cmd;
import vn.panota.base.bridge.mongodb.CompareQueryCmd;
import vn.panota.base.bridge.mongodb.LogicalQueryCmd;
import vn.panota.base.cache.ShareableCacheHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class AbstractDataService {

    protected Vertx vertx;
    protected ShareableCacheHolder cacheHolder;

    protected AbstractDataService(Vertx vertx) {
        this.vertx = vertx;
        SharedData sharedData = vertx.sharedData();
        LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
        this.cacheHolder = (ShareableCacheHolder) localDataAccess.get(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME);
    }

    protected String readSchemaFile(String name) {
        Buffer buffer = vertx.fileSystem().readFileBlocking(name);
        return buffer.toString();
    }

    protected JsonObject parseFilterToMongoQuery(JsonObject filter) {
        if (null == filter) return Cmd.empty();
        JsonObject query = null;
        if (filter.containsKey("$or")) {
            JsonArray orFilter = filter.getJsonArray("$or");
            List<JsonObject> orQuery = new ArrayList();
            for (int i = 0; i < orFilter.size(); i++) {
                orQuery.add(parseFilterToMongoQuery(orFilter.getJsonObject(i)));
            }
            query = LogicalQueryCmd.or(orQuery);
        } else if (filter.containsKey("$and")) {
            JsonArray andFilter = filter.getJsonArray("$and");
            List<JsonObject> andQuery = new ArrayList<>();
            for (int i = 0; i < andFilter.size(); i++) {
                andQuery.add(parseFilterToMongoQuery(andFilter.getJsonObject(i)));
            }
            query = LogicalQueryCmd.and(andQuery);
        } else {
            Set<String> strings = filter.fieldNames();
            for (String key : strings) {
                Object value = filter.getValue(key);
                if (value instanceof String) query = CompareQueryCmd.eq(key, value);
                else if (value instanceof JsonArray) query = CompareQueryCmd.in(key, ((JsonArray) value).getList());
                break;
            }
        }
        return query;
    }
}
