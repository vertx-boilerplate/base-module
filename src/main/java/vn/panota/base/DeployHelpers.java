package vn.panota.base;

import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.spi.cluster.zookeeper.ZookeeperClusterManager;
import vn.panota.base.annotation.Starter;
import vn.panota.base.bridge.mongodb.Cmd;
import vn.panota.base.connector.MongodbConnector;
import vn.panota.base.option.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DeployHelpers {

    private static Logger LOGGER = LoggerFactory.getLogger(MongodbConnector.class);

    public static Future<Void> deployHelper(Vertx vertx, Verticle verticle, DeploymentOptions deploymentOptions) {
        final Promise<Void> promise = Promise.promise();
        vertx.deployVerticle(verticle, deploymentOptions, res -> {
            if (res.failed()) {
                LOGGER.error("Failed to deploy Verticle " + verticle.getClass().getSimpleName());
                promise.fail(res.cause());
            } else {
                LOGGER.info("Deployed Verticle " + verticle.getClass().getSimpleName());
                promise.complete();
            }
        });
        return promise.future();
    }

    public static Future<Void> deployHelper(Vertx vertx, String name, DeploymentOptions config) {
        final Promise<Void> promise = Promise.promise();
        vertx.deployVerticle(name, config, res -> {
            if (res.failed()) {
                LOGGER.error("Failed to deploy Verticle " + name);
                promise.fail(res.cause());
            } else {
                LOGGER.info("Deployed Verticle " + name);
                promise.complete();
            }
        });
        return promise.future();
    }

    public static Future<Void> deployHelper(Vertx vertx, Class<? extends Verticle> clazz, DeploymentOptions options) {
        final Promise<Void> promise = Promise.promise();
        vertx.deployVerticle(clazz, options, res -> {
            if (res.failed()) {
                LOGGER.error("Failed to deploy verticle " + clazz.getSimpleName());
                promise.fail(res.cause());
            } else {
                LOGGER.info("Deployed verticle " + clazz.getSimpleName());
                promise.complete();
            }
        });
        return promise.future();
    }

    public static void deployMultiple(Vertx vertx, List<Class<? extends Verticle>> clazzes, Handler<AsyncResult<Void>> handler) {
        deployMultiple(vertx, clazzes, new DeploymentOptions(), handler);
    }

    public static void deployMultiple(Vertx vertx, List<Class<? extends Verticle>> clazzes, DeploymentOptions options, Handler<AsyncResult<Void>> handler) {
        Boolean asCluster = options.getConfig().getBoolean("as_cluster", false);
        final Promise<CompositeFuture> allComplete = Promise.promise();
        List<Class<? extends Verticle>> highestList = new ArrayList<>();
        List<Class<? extends Verticle>> highList = new ArrayList<>();
        List<Class<? extends Verticle>> mediumList = new ArrayList<>();
        List<Class<? extends Verticle>> lowList = new ArrayList<>();
        List<Class<? extends Verticle>> lowestList = new ArrayList<>();
        for (Class clazz : clazzes) {
            Starter anno = (Starter) clazz.getDeclaredAnnotation(Starter.class);
            Order value = Order.LOWEST;
            if (null != anno) value = anno.value();
            switch (value) {
                case HIGHEST:
                    highestList.add(clazz);
                    break;
                case HIGH:
                    highList.add(clazz);
                    break;
                case MEDIUM:
                    mediumList.add(clazz);
                    break;
                case LOW:
                    lowList.add(clazz);
                    break;
                case LOWEST:
                    lowestList.add(clazz);
                    break;
            }
        }
        Promise<CompositeFuture> highestFut = Promise.promise();
        if (highestList.isEmpty()) highestFut.complete();
        else CompositeFuture.all(handleDeploySet(vertx, asCluster, options, highestList)).setHandler(highestFut);
        highestFut.future().compose(v -> {
            Promise<CompositeFuture> highFut = Promise.promise();
            if (highList.isEmpty()) highFut.complete();
            else CompositeFuture.all(handleDeploySet(vertx, asCluster, options, highList)).setHandler(highFut);
            return highFut.future();
        }).compose(v -> {
            Promise<CompositeFuture> mediumFut = Promise.promise();
            if (mediumList.isEmpty()) mediumFut.complete();
            else CompositeFuture.all(handleDeploySet(vertx, asCluster, options, mediumList)).setHandler(mediumFut);
            return mediumFut.future();
        }).compose(v -> {
            Promise<CompositeFuture> lowFut = Promise.promise();
            if (lowList.isEmpty()) lowFut.complete();
            else CompositeFuture.all(handleDeploySet(vertx, asCluster, options, lowList)).setHandler(lowFut);
            return lowFut.future();
        }).compose(v -> {
            if (lowestList.isEmpty()) allComplete.complete();
            else
                CompositeFuture.all(handleDeploySet(vertx, asCluster, options, lowestList)).setHandler(allComplete);
        }, allComplete.future());
        allComplete.future().setHandler(ar -> {
            if (ar.succeeded()) handler.handle(Future.succeededFuture());
            else handler.handle(Future.failedFuture(ar.cause()));
        });
    }

    private static List<Future> handleDeploySet(Vertx vertx, boolean asCluster, DeploymentOptions options, List<Class<? extends Verticle>> deployList) {
        return deployList.stream()
                .map(c -> asCluster ? Future.future() : deployHelper(vertx, c, options))
                .collect(Collectors.toList());
    }
}
