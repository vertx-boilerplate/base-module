package vn.panota.base.util;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;

import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtil {

    public static void getConfigRetriever(String file, Vertx vertx, Handler<AsyncResult<JsonObject>> handler) {
        ConfigStoreOptions fileStore = new ConfigStoreOptions()
                .setType("file")
                .setConfig(new JsonObject().put("path", file));
        ConfigRetriever configRetriever = ConfigRetriever.create(vertx,
                new ConfigRetrieverOptions().addStore(fileStore));
        configRetriever.getConfig(handler);
    }

    public static void gzip(String absFilePath, String targetFilePath, Vertx vertx, Handler<AsyncResult<Void>> handler) {
        vertx.executeBlocking(event -> {
            try {
                InputStream in = Files.newInputStream(Paths.get(absFilePath));
                OutputStream fout = Files.newOutputStream(Paths.get(targetFilePath + ".gz"));
                BufferedOutputStream out = new BufferedOutputStream(fout);
                GzipCompressorOutputStream gzOut = new GzipCompressorOutputStream(out);
                final byte[] buffer = new byte[1024];
                int n = 0;
                while (-1 != (n = in.read(buffer))) {
                    gzOut.write(buffer, 0, n);
                }
                gzOut.close();
                in.close();
                event.complete();
            } catch (Exception ex) {
                event.fail(ex);
            }
        }, handler);
    }

}
