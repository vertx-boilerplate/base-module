package vn.panota.base.util;

import io.vertx.core.Vertx;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.Shareable;
import io.vertx.core.shareddata.SharedData;

public class VertxUtil {

    public static <T> T getLocalCacheObject(Vertx vertx, String mapName, String objectName) {
        SharedData sharedData = vertx.sharedData();
        LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(mapName);
        return (T) localDataAccess.get(objectName);
    }

    public static void setLocalCacheObject(Vertx vertx, String mapName, String objectName, Shareable object) {
        SharedData sharedData = vertx.sharedData();
        LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(mapName);
        localDataAccess.put(objectName, object);
    }
}
