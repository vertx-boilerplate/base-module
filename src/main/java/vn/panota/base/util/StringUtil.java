package vn.panota.base.util;

import io.vertx.core.json.JsonObject;
import vn.panota.base.bridge.mongodb.Cmd;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class StringUtil {

    /**
     * Remove accent from a string
     *
     * @param s
     * @return
     */
    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
                .replaceAll("đ", "d")
                .replaceAll("Đ", "D");
        return s;
    }

    /**
     * Convert Ipv4 to long number
     *
     * @param dottedIP
     * @return
     */
    public static Long Dot2LongIP(String dottedIP) {
        String[] addrArray = dottedIP.split("\\.");
        long num = 0;
        for (int i = 0; i < addrArray.length; i++) {
            int power = 3 - i;
            num += ((Integer.parseInt(addrArray[i]) % 256) * Math.pow(256, power));
        }
        return num;
    }

    public static String toString(String delimiter, Object... args) {
        StringBuilder sb = new StringBuilder();
        for (Object a : args) {
            if (null != a) sb.append(a).append(delimiter);
        }
        return sb.deleteCharAt(sb.length() - 1).toString();
    }

    public static String toString(Object... args) {
        StringBuilder s = new StringBuilder();
        for (Object arg : args) if (arg != null) s.append(arg);
        return s.toString();
    }

    public static String parseToSlug(String data) {
        return StringUtil.stripAccents(data.trim().toLowerCase()).replaceAll(" ","-");
    }

    public static int safeParseInt(String str) {
        return safeParseInt(str, 0);
    }

    public static int safeParseInt(String str, int def) {
        try {
            return Integer.valueOf(str);
        } catch (NumberFormatException ex) {
            return def;
        }
    }

    /**
     * Parse arguments from main method to map. If the keys set is empty, it will not be validate.
     * The arguments array must be divisible by 2.
     *
     * @param args
     * @param keys
     * @return
     */
    public static JsonObject parseMainArgs(String[] args, Set<String> keys) {
        JsonObject config = Cmd.empty();
        if (null != args && args.length > 0 && args.length % 2 == 0) {
            for (int i = 0; i < args.length; i += 2) {
                String key = args[i], value = args[i + 1];
                if (null != keys && !keys.isEmpty() && !key.contains(key)) {
                    throw new IllegalArgumentException(String.format("Key %s is out of scope in %s",
                            key, keys.stream().collect(Collectors.joining(","))));
                }
                config.put(key, value);
            }
        }
        return config;
    }
}
