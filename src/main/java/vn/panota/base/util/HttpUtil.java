package vn.panota.base.util;


import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;

/**
 * Created by thangpham on 15/07/2018.
 */
public class HttpUtil {

    private static Buffer INVALID_ACCOUNT = new JsonObject().put("message", "Invalid username or password").put("code", 400).toBuffer();
    private static Buffer UNMATCH_ACCOUNT = new JsonObject().put("message", "Username or Password do not match").put("code", 400).toBuffer();
    private static Buffer ERROR_CREATE_ACCOUNT = new JsonObject().put("message", "Error when create new account").put("code", 500).toBuffer();
    private static Buffer ERROR_UPDATE_ACCOUNT = new JsonObject().put("message", "Error when update account").put("code", 500).toBuffer();
    private static Buffer UNAUTHORISED_REQUEST = new JsonObject().put("message", "Unauthorised to request resource").put("code", 403).toBuffer();
    private static Buffer UNAUTHENTICATED_REQUEST = new JsonObject().put("message", "Unauthenticated to request resource").put("code", 401).toBuffer();
    private static Buffer ILLEGAL_INPUT_DATA = new JsonObject().put("message", "Illegal input data").put("code", 400).toBuffer();
    private static Buffer ERROR_SUBMIT_RESOURCE = new JsonObject().put("message", "Could not submit resource to system, please try again").put("code", 500).toBuffer();
    private static Buffer UNKNOWN_ERROR = new JsonObject().put("message", "Unknown error occurred").put("code", 500).toBuffer();
    private static Buffer ERROR_404 = new JsonObject().put("message", "404").put("code", 404).toBuffer();
    private static JsonObject OK_RAW = new JsonObject().put("message", "success").put("code", 200);
    private static Buffer OK = OK_RAW.toBuffer();

    public static void ERROR_404_RESPONSE(HttpServerResponse response) {
        response.putHeader(HttpHeaders.CONTENT_LENGTH, ERROR_404.length() + "")
                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setStatusCode(200)
                .end(ERROR_404);
    }

    public static void ILLEGAL_INPUT_DATA_RESPONSE(HttpServerResponse response) {
        response.putHeader("Content-Length", ILLEGAL_INPUT_DATA.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(ERROR_UPDATE_ACCOUNT);
    }

    public static void CUSTOM_ERROR_RESPONSE(HttpServerResponse response, String errorMessage) {
        Buffer toString = new JsonObject().put("message", errorMessage).toBuffer();
        response.putHeader("Content-Length", toString.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(toString);
    }

    public static void ERROR_UPDATE_ACCOUNT_RESPONSE(HttpServerResponse response) {
        response.putHeader("Content-Length", ERROR_UPDATE_ACCOUNT.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(ERROR_UPDATE_ACCOUNT);
    }

    public static void UNKNOWN_ERROR_RESPONSE(HttpServerResponse response) {
        response.putHeader("Content-Length", UNKNOWN_ERROR.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(UNKNOWN_ERROR);
    }

    public static void UNMATCH_ACCOUNT_RESPONSE(HttpServerResponse response) {
        response.putHeader("Content-Length", UNMATCH_ACCOUNT.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(UNMATCH_ACCOUNT);
    }

    public static void ERROR_CREATE_ACCOUNT_RESPONSE(HttpServerResponse response) {
        response.putHeader("Content-Length", ERROR_CREATE_ACCOUNT.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(ERROR_CREATE_ACCOUNT);
    }

    public static void OK_RESPONSE(HttpServerResponse response) {
        response.putHeader("Content-Length", OK.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(OK);
    }

    public static void OK_RESPONSE(Object data, HttpServerResponse response) {
        Buffer respData = OK_RAW.copy().put("data", data).toBuffer();
        response.putHeader("Content-Length", respData.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(respData);
    }

    public static void OK_RESPONSE(Object data, JsonObject meta, HttpServerResponse response) {
        Buffer respData = OK_RAW.copy().put("data", data).put("meta", meta).toBuffer();
        response.putHeader("Content-Length", respData.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(respData);
    }

    public static void INVALID_ACCOUNT_RESPONSE(HttpServerResponse response) {
        response.putHeader("Content-Length", INVALID_ACCOUNT.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(INVALID_ACCOUNT);
    }

    public static void UNAUTHORISED_REQUEST_RESPONSE(HttpServerResponse response) {
        response.putHeader("Content-Length", UNAUTHORISED_REQUEST.length() + "")
                .putHeader("Content-Type", "application/json")
                .write(UNAUTHORISED_REQUEST)
                .setStatusCode(200).end();
    }

    public static void UNAUTHENTICATED_REQUEST(HttpServerResponse response) {
        response.putHeader("Content-Length", UNAUTHENTICATED_REQUEST.length() + "")
                .putHeader("Content-Type", "application/json")
                .write(UNAUTHENTICATED_REQUEST)
                .setStatusCode(200).end();
    }

    public static void ERROR_SUBMIT_RESOURCE_RESPONSE(HttpServerResponse response) {
        response.putHeader("Content-Length", ERROR_SUBMIT_RESOURCE.length() + "")
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(ERROR_SUBMIT_RESOURCE);
    }
}
