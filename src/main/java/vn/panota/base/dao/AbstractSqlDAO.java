package vn.panota.base.dao;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.SQLRowStream;
import vn.panota.base.Constants;
import vn.panota.base.cache.ShareableCacheHolder;

public abstract class AbstractSqlDAO {

    private static Logger LOGGER = LoggerFactory.getLogger(AbstractSqlDAO.class);

    protected Vertx vertx;
    protected ShareableCacheHolder cacheHolder;
    protected SQLClient sqlClient;
    protected SQLConnection sqlConnection;

    protected AbstractSqlDAO(Vertx vertx, String connName) {
        this.vertx = vertx;
        SharedData sharedData = vertx.sharedData();
        LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
        this.cacheHolder = (ShareableCacheHolder) localDataAccess.get(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME);
        sqlClient = this.cacheHolder.getSQLClient(connName);
        sqlClient.getConnection(event -> {
           if(event.succeeded()) this.sqlConnection = event.result();
           else LOGGER.error("Could not get sql connection from " + connName, event.cause());
        });
    }

    protected void fetchData(String query, Handler<AsyncResult<SQLRowStream>> handler) {
        sqlClient.queryStream(query, handler);
    }

    protected void getOneRecord(String query, Handler<AsyncResult<JsonArray>> handler) {
        sqlClient.querySingle(query, handler);
    }

    protected void getData(String query, Handler<AsyncResult<ResultSet>> handler) {
        sqlClient.query(query, handler);
    }
}
