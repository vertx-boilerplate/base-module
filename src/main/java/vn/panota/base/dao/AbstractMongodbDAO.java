package vn.panota.base.dao;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.MongoClientUpdateResult;
import io.vertx.ext.mongo.UpdateOptions;
import vn.panota.base.Constants;
import vn.panota.base.bridge.mongodb.*;
import vn.panota.base.cache.ShareableCacheHolder;
import vn.panota.base.util.VertxUtil;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by thangpham on 20/12/2018.
 */
public abstract class AbstractMongodbDAO {

    public static String LAST_UPDATED = "last_updated";
    public static String CREATED_AT = "created_at";

    protected Vertx vertx;
    protected String collection;
    protected ShareableCacheHolder cacheHolder;
    protected MongoClient mongoClient;

    protected AbstractMongodbDAO(Vertx vertx, String conName, String collection) {
        this.vertx = vertx;
        this.collection = collection;
        this.cacheHolder = (ShareableCacheHolder) VertxUtil.getLocalCacheObject(vertx, Constants.SharedDataName.SHARED_MODULAR_LOCAL, Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME);
        this.mongoClient = this.cacheHolder.getMongoClient(conName);
    }

    public abstract <T extends AbstractMongodbDAO> T init();

    public abstract void setupCollection(Handler<AsyncResult<Void>> handler);

    public void count(JsonObject filter, Handler<AsyncResult<Long>> handler) {
        mongoClient.count(collection, filter, handler);
    }

    protected void fetchData(JsonObject query, JsonObject sort, int skip, int limit,
                             Handler<AsyncResult<List<JsonObject>>> handler) {
        FindOptions findOptions = new FindOptions().setSkip(skip).setLimit(limit);
        if (null != sort) findOptions.setSort(sort);
        mongoClient.findWithOptions(collection, query, findOptions, ar -> {
            if (ar.succeeded()) {
                List<JsonObject> resultList = ar.result();
                resultList.forEach(this::parseExtensionFields);
                handler.handle(Future.succeededFuture(resultList));
            } else handler.handle(Future.failedFuture(ar.cause()));
        });
    }

    protected void findOne(JsonObject query, Handler<AsyncResult<JsonObject>> handler) {
        this.findOne(query, null, handler);
    }

    protected void findOne(JsonObject query, JsonObject fields, Handler<AsyncResult<JsonObject>> handler) {
        mongoClient.findOne(collection, query, fields, ar -> {
            if (ar.succeeded()) {
                JsonObject result = ar.result();
                if (null != result) parseExtensionFields(result);
                handler.handle(Future.succeededFuture(result));
            } else handler.handle(Future.failedFuture(ar.cause()));
        });
    }

    protected void createOne(JsonObject data, Handler<AsyncResult<JsonObject>> handler) {
        Instant instant = new Date().toInstant();
        data.put(CREATED_AT, instant);
        data.put(LAST_UPDATED, instant);
        mongoClient.insert(collection, data, ar -> {
            if (ar.succeeded()) {
                JsonObject newData = parseExtensionFields(Cmd.empty().mergeIn(data));
                newData.put("id", ar.result());
                handler.handle(Future.succeededFuture(newData));
            } else handler.handle(Future.failedFuture(ar.cause()));
        });
    }

    protected void upsertOne(JsonObject query, JsonObject update, Handler<AsyncResult<JsonObject>> handler) {
        Instant instant = new Date().toInstant();
        update.getJsonObject("$set").put(LAST_UPDATED, instant);
        update.getJsonObject("$setOnInsert").put(CREATED_AT, instant);
        mongoClient.updateCollectionWithOptions(collection, query, update,
                new UpdateOptions().setUpsert(true).setReturningNewDocument(true), ar -> {
                    if (ar.succeeded()) {
                        JsonObject object = parseExtensionFields(ar.result().toJson());
                        handler.handle(Future.succeededFuture(object));
                    } else handler.handle(Future.failedFuture(ar.cause()));
                });
    }

    protected void editOne(JsonObject query, JsonObject data, JsonObject fields, Handler<AsyncResult<JsonObject>> handler) {
        FindOptions findOptions = new FindOptions();
        if (null != fields) findOptions.setFields(fields);
        mongoClient.findOneAndUpdateWithOptions(collection, query, data, findOptions,
                new UpdateOptions().setReturningNewDocument(true), ar -> {
                    if (ar.succeeded()) {
                        JsonObject object = parseExtensionFields(ar.result());
                        handler.handle(Future.succeededFuture(object));
                    } else handler.handle(Future.failedFuture(ar.cause()));
                });
    }

    protected void editMultiple(JsonObject query, JsonObject data, Handler<AsyncResult<MongoClientUpdateResult>> handler) {
        mongoClient.updateCollectionWithOptions(collection, query, FieldUpdate.set(data),
                new UpdateOptions().setMulti(true).setReturningNewDocument(true), handler);
    }

    protected void deleteOne(JsonObject query, Handler<AsyncResult<JsonObject>> handler) {
        mongoClient.findOneAndDelete(collection, query, ar -> {
            if (ar.succeeded()) {
                JsonObject object = parseExtensionFields(ar.result());
                handler.handle(Future.succeededFuture(object));
            } else handler.handle(Future.failedFuture(ar.cause()));
        });
    }

    public void deleteMultiple(JsonObject query, Handler<AsyncResult<Long>> handler) {
        mongoClient.removeDocuments(collection, query, ar -> {
            if (ar.succeeded()) handler.handle(Future.succeededFuture(ar.result().getRemovedCount()));
            else handler.handle(Future.failedFuture(ar.cause()));
        });
    }

    protected JsonObject parseExtensionFields(JsonObject data) {
        if (null == data) return null;
        data.fieldNames().forEach(field -> {
            Object value = data.getValue(field);
            if (value instanceof JsonObject) {
                JsonObject object = (JsonObject) value;
                if (object.containsKey("$date")) data.put(field, object.getString("$date"));
                else if (object.containsKey("$oid")) data.put(field, object.getString("$oid"));
                else if (field.equals("upserted_id")) data.put(field, object.getJsonObject("_id").getString("$oid"));
            }
        });
        if (data.containsKey("_id")) {
            data.put("id", data.getString("_id"));
            data.remove("_id");
        }
        return data;
    }

    protected JsonObject convertToQuery(JsonArray filters) {
        if (null == filters || filters.isEmpty()) return Cmd.empty();
        List<JsonObject> or = new ArrayList<>();
        for (int i = 0; i < filters.size(); i++) {
            JsonObject filter = filters.getJsonObject(i);
            if (filter.isEmpty()) continue;
            List<JsonObject> and = new ArrayList<>();
            or.add(LogicalQueryCmd.or(and));
            filter.fieldNames().forEach(field -> {
                Object value = filter.getValue(field);
                String[] s = field.split("_");
                if (field.endsWith("_ct")) and.add(EvaluateQueryCmd.contains(s[0], filter.getString(field)));
                else if (field.endsWith("_gt")) and.add(CompareQueryCmd.gt(s[0], value));
                else if (field.endsWith("_gte")) and.add(CompareQueryCmd.gte(s[0], value));
                else if (field.endsWith("_lt")) and.add(CompareQueryCmd.lt(s[0], value));
                else if (field.endsWith("_lte")) and.add(CompareQueryCmd.lte(s[0], value));
                else if (value instanceof JsonArray) and.add(CompareQueryCmd.in(field, ((JsonArray) value).getList()));
                else and.add(CompareQueryCmd.eq(field, value));
            });
        }
        return LogicalQueryCmd.or(or);
    }
}
