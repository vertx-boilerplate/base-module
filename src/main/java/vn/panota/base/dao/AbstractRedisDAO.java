package vn.panota.base.dao;

import io.vertx.core.Vertx;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.redis.client.RedisAPI;
import vn.panota.base.Constants;
import vn.panota.base.cache.ShareableCacheHolder;

public abstract class AbstractRedisDAO {

    protected Vertx vertx;
    protected ShareableCacheHolder cacheHolder;
    protected RedisAPI redisAPI;

    protected AbstractRedisDAO(Vertx vertx) {
        this.vertx = vertx;
        SharedData sharedData = vertx.sharedData();
        LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
        this.cacheHolder = (ShareableCacheHolder) localDataAccess.get(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME);
        this.redisAPI = this.cacheHolder.getRedisAPI();
    }


}
