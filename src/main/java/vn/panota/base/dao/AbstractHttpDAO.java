package vn.panota.base.dao;

import io.vertx.core.Vertx;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.web.client.WebClient;
import vn.panota.base.Constants;
import vn.panota.base.cache.ShareableCacheHolder;

public abstract class AbstractHttpDAO {

    protected Vertx vertx;
    protected ShareableCacheHolder cacheHolder;
    protected WebClient webClient;

    protected AbstractHttpDAO(Vertx vertx) {
        this.vertx = vertx;
        SharedData sharedData = vertx.sharedData();
        LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
        this.cacheHolder = (ShareableCacheHolder) localDataAccess.get(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME);
        this.webClient = this.cacheHolder.getWebClient();
    }
}
