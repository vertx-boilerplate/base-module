package vn.panota.base;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import vn.panota.base.util.FileUtil;

public class ServiceInitializer extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(ServiceInitializer.class);

    @Override
    public void start(Future<Void> handler) throws Exception {
        LOGGER.info("START to load service");
        FileUtil.getConfigRetriever(Constants.ConfigurationFile.SERVICE_CONFIG, vertx, ar -> {
            if(ar.succeeded()) {
                EventBus eventBus = vertx.eventBus();
            }
        });
    }
}
