package vn.panota.base;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.web.Router;
import vn.panota.base.annotation.Starter;
import vn.panota.base.cache.ShareableCacheHolder;
import vn.panota.base.option.Order;
import vn.panota.base.util.FileUtil;

@Starter(Order.HIGHEST)
public class AppConfigInitializer extends AbstractVerticle {

    private static Logger LOGGER = LoggerFactory.getLogger(LocalCacheInitializer.class);

    private static Router router;

    public static Router getRouter() {
        return router;
    }

    @Override
    public void start(Promise<Void> handler) throws Exception {
        LOGGER.info("START to load App configuration");
        router = Router.router(vertx);
        FileUtil.getConfigRetriever(Constants.ConfigurationFile.APP_CONFIG, vertx, cfAr -> {
            if (cfAr.succeeded()) {
                JsonObject config = cfAr.result();
                SharedData sharedData = vertx.sharedData();
                LocalMap<String, Object> localDataAccess = sharedData.getLocalMap(Constants.SharedDataName.SHARED_MODULAR_LOCAL);
                localDataAccess.put(Constants.SharedDataName.SHARED_MODULAR_CACHE_NAME, ShareableCacheHolder._load().setAppConfig(config));
                LOGGER.info("LOAD App-Config SUCCESSFULLY");
                handler.complete();
            } else handler.fail(cfAr.cause());
        });
    }
}
