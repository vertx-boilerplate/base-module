package vn.panota.base;

public interface Constants {

    interface SharedDataName {
        String SHARED_MODULAR_LOCAL = "SHARED_LOCAL";
        String SHARED_MONGODB_PATTERN = "MONGODB_";
        String SHARED_MODULAR_CACHE_NAME = "SHARED_MODULAR_CACHE_NAME";
    }

    interface CacheName {
        String MONGODB_AUTH = "SHARED_MONGO_AUTH";
        String JWT_AUTH = "SHARED_JWT_AUTH";
    }

    interface ConfigurationFile {
        String APP_CONFIG = "./config/app-config.json";
        String HTTP_SERVER_CONFIG = "./config/http-server-config.json";
        String HTTPS_SERVER_CONFIG = "./config/https-server-config.json";
        String MONGODB_CONNECTION_CONFIG = "./config/mongodb-connection-config.json";
        String REDIS_CONFIG = "./config/redis-config.json";
        String SQL_CONNECTION_CONFIG = "./config/sql-connection-config.json";
        String SERVICE_CONFIG = "./config/service-providers.json";
        String KAFKA_CONSUMER_CONFIG = "./config/kafka-consumer-config.json";
        String KAFKA_PRODUCER_CONFIG = "./config/kafka-producer-config.json";
    }

    interface JobKey {
        String TIME_FRAME = "time_frame";
        String FROM_TIME = "from_time";
        String TO_TIME = "to_time";
        String LAST_RUN_TIME = "last_run";
        String HOURLY = "hourly";
        String DAILY = "daily";
    }

    interface ResponseFields {
        String CODE = "code";
        String TOKEN = "token";
        String DATA = "data";
    }

    interface RequestFields {
        String LIMIT = "limit";
        String FILTER = "filter";
        String SORT = "sort";
        String PAGE = "page";
        String SIZE = "size";
    }
}
